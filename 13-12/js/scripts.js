document.addEventListener('DOMContentLoaded', onReady);

/**
 * @desc We start from here!!!!
 **/
function onReady() {
    const btnAddClient = document.getElementById('addClient');

    btnAddClient.onclick = function() {
        const clientName = prompt('Имя клиента?', '');

        if (clientName) {
            Cl.add(clientName);
            const clientList = Cl.get();

            buildList(clientList, document.getElementById('clientList'));

            // if (Cl.remove(clientName)) {
            //     alert('We did it!!!');
            // } else {
            //     alert('Something went wrong!!!');
            // }

        }
    };




    // const btnListClient = document.getElementById('listClient');
    //
    // const clients = ['one', 'two', 'three'];
    // const clientList = document.getElementById('clientList')
    //     ? document.getElementById('clientList')
    //     : document.createElement('ul');
    //
    // list(clients, clientList);
    //
    // document.body.appendChild(clientList);
    //
    // btnAddClient.click = function() {
    //     alert('Add client!!');
    // };
    //
    // btnListClient.click = function() {
    //     alert('List client!!!');
    // };
}


/**
 * @param {Array}
 * @param {Object}
 * @return {Boolean}
 **/
function buildList(list, container) {
    if (!Array.isArray(list)) {
        return false;
    }

    container.innerHTML = '';

    list.forEach(function(name) {
        const li = document.createElement('li');
        li.innerHTML = name;

        container.appendChild(li);
    });

    return true;
}


/**
 * @desc Output the array
 * @return {Node}
 **/
function list(items, parent = null) {
    const ul = parent ? parent : document.createElement('ul');

    items.forEach(function(item) {
        const li = document.createElement('li');

        li.innerHTML = item;

        ul.appendChild(li);
    });

    return ul;
}