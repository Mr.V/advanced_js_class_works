function Human(firstName, lastName) {
    this.firstName = firstName;
    this.lastName = lastName;

    this.fullname = function(to = '1') {

        console.log(arguments);

        return `${this.firstName} ${this.lastName}`;
    };

    this.sayHello = prefix => `${prefix}-${this.firstName}`;
}