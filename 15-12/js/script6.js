var data = {};

function MacDonalds(size, stuffing) {
    this.size = size;
    this.stuffing = stuffing;
    this.toppings = [];
}

MacDonalds.SIZE_SMALL = 'size_small';
MacDonalds.SIZE_LARGE = 'size_large';

MacDonalds.STUFFING_CHEESE = 'stuff_cheese';

MacDonalds.TOPPING_MAYO = 'topping_mayo';

MacDonalds.prototype.addTopping = function (topping) {
    this.toppings.push(topping);
};
MacDonalds.prototype.removeTopping = function (topping) {
    for (var i = 0; i < this.toppings.length; i++) {
        if (this.toppings[i] === topping) {
            this.toppings.splice(i, 1);
        }
    }
};
MacDonalds.prototype.getToppings = function () {
    return this.toppings;
};

MacDonalds.prototype.getSize = function () {
    return this.size;
};
MacDonalds.prototype.getStuffing = function () {
    return this.stuffing
};

MacDonalds.prototype.calculatePrice = function () {
    var result = 0;
    var priceSize = data[this.size];
    var priceStuff = data[this.stuffing];


    // Check what i s priceStuff
    result += priceSize.price;
    result += priceStuff.price;

    for(var i = 0; i < this.toppings.length; i++) {
        result += data[this.toppings[i]]['price'];
    }

    return result;
};
MacDonalds.prototype.calculateCalories = function () {



};




///
data[MacDonalds.SIZE_SMALL] = {
    price: 12,
    callories: 20
};
data[MacDonalds.SIZE_LARGE] = {
    price: 30,
    callories: 50
};
data[MacDonalds.STUFFING_CHEESE] = {
    price: 30,
    callories: 50
};

data[MacDonalds.TOPPING_MAYO] = {
    price: 10,
    callories: 10
};


var hamb = new MacDonalds(
    MacDonalds.SIZE_SMALL,
    MacDonalds.STUFFING_CHEESE
);
