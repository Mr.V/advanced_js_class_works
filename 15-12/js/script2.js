const humans = {
    name: '',
    height: 150,
    age: 12,
    gender: 'female',
    hasHorns: false,
    canFly: false
};

const orcs = {
    name: '',
    height: 250,
    age: 120,
    gender: 'male',
    hasHorns: true,
    canFly: false
};

const hegety = {
    name: '',
    height: 250,
    age: 120,
    gender: 'none',
    hasHorns: true,
    canFly: true
};

// ------

const ivan = Object.create(humans);

ivan.name = 'Ivan';
ivan.age = 37;
ivan.gender = 'male';

const illiya = Object.create(humans);

illiya.name = 'Illiya';
illiya.age = 37;
illiya.gender = 'male';

const army = [ivan, illiya];

console.log(army);