const person1 = {
    name: 'Ivanov Ivan Ivanovich',
    age: 54,
    city: 'Kyiv',
    sayHello: function(to = 'you') {
        console.log(`${this.name} has just said hello to ${to}!!!`);
    }
};

const person2 = {
    name: 'Alexsey Aleksandrovich Sidorov',
    age: 76,
    city: 'Kyiv',
    sayHello: function(to = 'you') {
        console.log(`${this.name} has just said Hello to ${to}!!!`);
    }
};

const person3 = {
    name: 'Yulia Vladimirovna Timoshenkova',
    age: 95,
    city: 'Moscow',
    sayHello: function(to = 'you') {
        return `${this.name} has just said Hello to ${to}!!!`;
    }
};

const clients = [person1, person2, person3];

clients.forEach(function(client) {

    doSomething.call(client, 'Sergey');

});


function doSomething(to = 'me') {
    console.log(`Some information from ${this.name}, to ${to} and ${this.sayHello()}`);
}

const newFunction = doSomething.bind(person1);

newFunction('Sergey');