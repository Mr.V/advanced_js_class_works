/**
 *
 **/
function Creature({name, age, height, gender}) {
    this.name = name;
    this.age = age;
    this.height = height;
    this.gender = gender;

    this.canWork = false;
}

Creature.prototype.canWalk = function() {
    return true;
};

Creature.prototype.toSpeak = function() {
    return 'ooops';
};

/**
 *
 **/
function Human({name, age, iq = 50}) {
    Creature.call(this, {name, age});

    this.canWork = true;
    this.iq = iq;
}

Human.prototype = Creature.prototype;


function Ork({name, age, hornAmount}) {
    Creature.call(this, {name, age});

    this.hornAmount = hornAmount;
}

Ork.prototype = Creature.prototype;

Ork.prototype.isKing = function() {
    return this.hornAmount > 2;
};


const h = new Human({iq: 123, name: 'Viktor', age: 18});

const ork = new Ork({name: 'Ahbe', age: 188, hornAmount: 1});
const ork2 = new Ork({name: 'Bibi', age: 188, hornAmount: 10});